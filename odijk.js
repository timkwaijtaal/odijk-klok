// deze functie start de timer van hoe lang de tijd duurt.
function startTimer(duration, display) {
    var timer = duration, minutes, seconds;
    setInterval(function () {
        minutes = parseInt(timer / 60, 10)
        seconds = parseInt(timer % 60, 10);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        display.textContent = minutes + ":" + seconds;

        if (--timer < 0) {
            timer = duration;
        }
    }, 1000);
}
// hieronder zie je een functie van een tijsloop die actief wordt
window.onload = function () {
    var fiveMinutes = 60 * 120,
        display = document.querySelector('#time');
    startTimer(fiveMinutes, display);
};


$( '#done' ).click( function()
{
    
  alert( $( '#netflix' ).attr( 'srcdoc' ) );
    
});